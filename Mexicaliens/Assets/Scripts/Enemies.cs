﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemies : MonoBehaviour
{
    private bool blocked = false;
    public string positions;
    private int i;
    public int damage;
    public int coinsValue;
    private float timer;
    public float rateOfAttack;
    public float speed;
    public float hp;
    public LayerMask Fighter;
    public Collider ThisCollider;
    public GameObject center;
    public GameObject[] positionPassed;
    public GameObject[] nextPosition;

    // at the start, the enemy is pointed toward the first position
    void Start ()
    {
        ThisCollider = GetComponent<Collider>();
        timer = rateOfAttack;
        positionPassed = new GameObject[nextPosition.Length];
        transform.LookAt(nextPosition[i].transform);
    }

	void Update ()
    {
        if (!blocked)
            Movement();
        else
            AttackFighter();

        CheckEnemy();

        if (hp <= 0)
            DestroyEnemy();
    }

    //the enemy continues to go forward the nextPosition that it is pointed to

    void Movement()
    {
        transform.position += transform.forward * Time.deltaTime * speed;
        //metti il TRANSFORM.LERP!!!!!
    }

    //if the health of the enemy reaches 0,it is destroyed, and increases the number of coins and the number of the enemy destroyed

    void DestroyEnemy()
    {
        GameManager.instance.coins += coinsValue;
        GameManager.instance.coinsText.text = "" + GameManager.instance.coins;
        GameManager.instance.enemiesCounter++;
        if (GameManager.instance.enemiesCounter >= GameManager.instance.numberOfEnemies)
            GameManager.instance.Win();
        Destroy(this.gameObject);
    }

    void CheckEnemy()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward,out hit, 2, Fighter))
        {
            if(hit.collider.GetComponent<FighterActions>().enemiesColliders[0]!=ThisCollider)
                blocked = true;
        }
        else
            blocked = false;
    }

    void AttackFighter()
    {
        timer += Time.deltaTime;
        if(timer >= rateOfAttack)
        {
            RaycastHit hit;
            Physics.Raycast(transform.position, transform.forward, out hit, 3, Fighter);
            hit.collider.GetComponent<FighterActions>().Hp -= damage;
            timer = 0;
        }
    }
    //when an enemy reaches a position,the enemy is pointed towards the next,and the record of the position passed is updated

    private void OnTriggerEnter(Collider other)
    {
        if (positionPassed[0] == null)
        {
            if (other.gameObject.tag == positions)
            {
                positionPassed[i] = other.gameObject;
                i++;
                transform.LookAt(nextPosition[i].transform);
            }
        }
        else if (other.gameObject.tag == positions && other.gameObject != positionPassed[i - 1])
        {
            positionPassed[i] = other.gameObject;
            i++;
            transform.LookAt(nextPosition[i].transform);
        }
    }

    //when the enemy reaches the wall,it deals a damage to the wall,then the enemy is destroyed

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "Wall")
        {
            GameManager.instance.actualHealth -= damage;
            GameManager.instance.UpdateHealthText(); 
            Destroy(this.gameObject);
        }
    }
}
