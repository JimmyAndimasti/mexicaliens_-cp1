﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretShooting : MonoBehaviour
{
    public float damageTurret;
    public float range;
    public int cost;

    private Quaternion rotationBuffer;
    public float rateOfFire;
    public float timer = 0;
    private Vector3 dimentions;
    public LayerMask enemies;
    public Transform spawnPoint;
    private Collider target;
    public Collider[] colliders;
    private Transform wallPosition;
    public GameObject bulletPrefab;
    public GameObject rangeViewer;

    private void Start()
    {
        wallPosition = GameManager.instance.wallPosition;
        //RangeViewerActivation();
    }
    //are recalled the AutoDetect and Timer methods

    void Update ()
    {
        if (target == null)
            AutoDetect();
        else
        {
            transform.LookAt(target.transform);
            if (timer > rateOfFire)
            {
                Shoot();
                timer = 0;
            }
        }
        CheckDistance();
        Timer();
        rotationBuffer = transform.rotation;
        rotationBuffer.x = 0;
        rotationBuffer.z = 0;
        transform.rotation = rotationBuffer;
    }

    //detect if an enemy is within range; if it is, check which one is the more advanced toward the end of the map,and it is made the target




    void AutoDetect()
    {
        colliders = Physics.OverlapSphere(transform.position, range, enemies);
        if (colliders[0] == null)
            ;


        else if (colliders != null)
        {
            target = colliders[0];

            for (int i = 0; i < colliders.Length; i++)
            {

                if (Vector3.Distance(target.transform.position, wallPosition.position) > Vector3.Distance(colliders[i].transform.position, wallPosition.position))
                    target = colliders[i];
            }
            transform.LookAt(target.transform);

            //once the timer reaches the rateOfFire, a bullet is shot, and the timer is reset to 0

            if (timer > rateOfFire)
            {
                Shoot();
                timer = 0;
            }
        }
    }

  

    //is shoot a bullet

    void Shoot()
    {
        GameObject bullet = Instantiate(bulletPrefab, spawnPoint.position, spawnPoint.rotation) as GameObject;
        bullet.GetComponent<Bullet>().target = target.GetComponent<Enemies>().center;
        bullet.GetComponent<Bullet>().damage = damageTurret;
    }

    //the timer increases

    void Timer()
    {
        timer += Time.deltaTime;
    }
    void CheckDistance()
    {
        if (Vector3.Distance(target.transform.position, transform.position) >= range)
            target = null;
    }
    void RangeViewerActivation()
    {
        GameObject RangeViewer = Instantiate(rangeViewer, transform.position, transform.rotation) as GameObject;
        dimentions.x = range * 2;
        dimentions.z = range * 2;
        dimentions.y = 0.5f;
        RangeViewer.transform.localScale = dimentions;
    }

   
}
