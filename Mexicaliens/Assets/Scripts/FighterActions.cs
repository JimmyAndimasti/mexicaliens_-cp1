﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FighterActions : MonoBehaviour
{
    public bool fighting = false;
    private float timer;
    public float Hp;
    public float rateOfAttacks;
    public float damage;
    public LayerMask enemies;
    public Collider[] enemiesColliders;
    public GameObject TurretSpawner;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Hp <= 0)
            Death();

        enemiesColliders = Physics.OverlapBox(transform.position, new Vector3(3, 2, 3), transform.rotation, enemies);
        if (enemiesColliders[0] != null)
            fighting = true;

        if (fighting)
            Timer();
    }
    void Timer()
    {
        timer += Time.deltaTime;
        if (timer >= rateOfAttacks)
            Attack();
    }
    void Attack()
    {
        //enemiesColliders = Physics.OverlapBox(transform.position, new Vector3(3, 2, 3), transform.rotation, enemies);
        if ((enemiesColliders[0].GetComponent<Enemies>().hp - damage) <= 0)
            fighting = false;
        enemiesColliders[0].GetComponent<Enemies>().hp -= damage;
        timer = 0;
    }
    void Death()
    {
        TurretSpawner.GetComponent<TurretSpawner>().unitSpawned = false;
        Destroy(this.gameObject);
    }
}
