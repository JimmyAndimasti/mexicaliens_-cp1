﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseOver : MonoBehaviour,
    IPointerEnterHandler,
    IPointerExitHandler
{
    private bool isPointed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        isPointed = true;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        //TurretSpawnMenu.instance.DeselectMenu();
    }

}
