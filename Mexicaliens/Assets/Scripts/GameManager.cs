﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    private bool paused = false;
    private bool gameOver = false;
    public bool startFirstWave = false;
    public bool waitForNewRound = false;

    public int enemiesCounter;
    public int numberOfEnemies;
    public int coins;
    public float timer;
    public float timeBetweenWave;
    public float actualHealth;
    public float maxHealth;

    public Text WallHealth;
    public Text coinsText;
    public Text gameOverText;
    public Text pauseText;

    public Transform wallPosition;
    public GameObject[] SpawnEnemies;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        coinsText.text = ""+ coins;
        actualHealth = maxHealth;
        WallHealth.text = actualHealth + "/" + maxHealth;
    }

    //the game quits when the O is pressed. when all the wave has been spawned,the timer is recalled. if the wall life reaches 0, the GameOver is recalled. 
    //when the game is in gameover,if the R is pressed, the game restarts. when the startfirst button is clicked,the first wave is spawned.

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.O))
            Application.Quit();

        //if (Input.GetKeyDown(KeyCode.A))
        //{
        //    coins += 100;
        //    coinsText.text = "" + coins;
        //}

        if(waitForNewRound)
        {
            Timer();
        }

        if (actualHealth < 0)
            GameOver();

        if(gameOver)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                Time.timeScale = 1;
            }
        }

        //if(!startFirstWave)
        //{
        //    if (startFirstWave)
        //    {
        //        StartFirstWave();
        //    }
        //}
    }
    
    // the timer is increased, when the timer reaches timeBetweenWave,the timer is reset and a new wave is started

    public void Timer()
    {
        timer += Time.deltaTime;
        if (timer > timeBetweenWave)
        {
            timer = 0;
            NewWave();
        }
    }

    // if the NewWave button is pressed,the number of coins is increased,and a new wave is started

    public void NewWave()
    {
        if (timer < timeBetweenWave)
        {
            coins += (int)timeBetweenWave - (int)timer;
            coinsText.text = "" + coins;
        }
        waitForNewRound = false;
        for (int i = 0; i < SpawnEnemies.Length; i++)
            SpawnEnemies[i].GetComponent<SpawnEnemies>().spawn = true;
    }

    // if the wall health reaches 0,the game is set in gameover

    public void GameOver()
    {
        Time.timeScale = 0;
        gameOverText.text = "YOU LOST";
        gameOver = true;
    }

    // if the player kills all the enemies,the game is set in gameover

    public void Win()
    {
        Time.timeScale = 0;
        gameOverText.text = "YOU WON";
        gameOver = true;
    }

    // if the startFirstWave button is pressed,the first wave is started

    public void StartFirstWave()
    {
        for (int i = 0; i < SpawnEnemies.Length; i++)
        {
            SpawnEnemies[i].GetComponent<SpawnEnemies>().spawn = true;
            SpawnEnemies[i].GetComponent<SpawnEnemies>().startTimer = true;
        }
        //startFirstWave = true;
    }
    public void UpdateHealthText()
    {
        WallHealth.text = actualHealth + " / " + maxHealth;
    }
}
