﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private bool inPause = false;
    public Button NewWave;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!inPause)
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                OptionMenu();
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                OptionMenu();
        }
        if (GameManager.instance.waitForNewRound)
            NewWave.gameObject.SetActive(true);
        else
            NewWave.gameObject.SetActive(false);
    }
    
     public void OptionMenu()
    {
        if (!inPause)
        {
            Time.timeScale = 0;
            inPause = true;
        }
        else if(inPause)
        {
            Time.timeScale = 1;
            inPause = false;
        }
    }
    public void NextWave()
    {
        GameManager.instance.timer = GameManager.instance.timeBetweenWave + 1;
    }
    public void FirstWave()
    {
        GameManager.instance.startFirstWave = true;
    }
}
