﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartingUI : MonoBehaviour
{
    public GameObject StartingUIObject;

    public void Deactivate()
    {
        GameManager.instance.StartFirstWave();
        StartingUIObject.SetActive(false);
    }
}
