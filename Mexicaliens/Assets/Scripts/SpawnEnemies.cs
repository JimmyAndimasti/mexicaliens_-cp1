﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemies : MonoBehaviour
{
    private bool loadComplete = false;
    public bool startTimer = false;
    public bool spawn = false;

    public string positionsType;
    private int i;
    private int j;
    private float timer;
    public float timeOfSpawn;

    public GameObject[] positions;
    public GameObject[] waveInfo;

    private void Start()
    {
        i = 0;
        j = 0;
    }

    //if the timer reaches timeOfSpawn and the spawn is true,the Spawn is recalled

    void Update ()
    {
        if (startTimer)
            Timer();
        if (timer > timeOfSpawn && spawn)
            Spawn();
	}

    //a enemy is spawned,then timer is reset to 0. when all the wave is spawned,the waitForNewWave in the gameManager is set to true

    void Spawn()
    {
        GameObject enemy = Instantiate(waveInfo[i].GetComponent<WaveInfo>().enemiesToSpawn[j], transform.position,transform.rotation) as GameObject;
        enemy.GetComponent<Enemies>().nextPosition = positions;
        enemy.GetComponent<Enemies>().positions = positionsType;
        j++;
        timer = 0;
        if (j >= waveInfo[i].GetComponent<WaveInfo>().enemiesToSpawn.Length)
        {
            j = 0;
            spawn = false;
            GameManager.instance.waitForNewRound = true;
            i++;
        }
    }

    //the timer increases

    void Timer()
    {
         timer += Time.deltaTime;
    }
}
