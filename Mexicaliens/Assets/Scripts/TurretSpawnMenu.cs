﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TurretSpawnMenu : MonoBehaviour
{
    public enum turretTypesEnum {moneyLv1,sauceLv1,weaponLv1,fighterLv1,moneyLv2,sauceLv2,weaponLv2,fighterLv2};
    public bool deactivate; 
    public static TurretSpawnMenu instance;
    private int turretNumber;
    private int currentTurretType;
    public int costTurret;
    public int[] turretTypes;
    private Transform fighterTurretSpawn;
    public GameObject turretPrefab;
    public GameObject[] turretPrefabUpgrades;
    private GameObject buttonsBaseTurret;
    private GameObject buttonsTurretPlaced;
    private GameObject buttonFather;
    public GameObject[] buttonsArray;
    public GameObject[] turretPlaced;
    private int [] stateTurrets;

    private void Update()
    {
        //if (Input.GetMouseButtonDown(0))
        //{
        //    RaycastHit hit;
        //    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //    if (Physics.Raycast(ray, out hit))
        //    {
        //        if (hit.transform.tag != "Buttons" || hit.transform.tag == null)
        //            DeselectMenu();
        //    }
        //}
    }

    public void TurretSpawn(Transform turretPosition)
    {
        if (turretPrefab.GetComponent<TurretShooting>().cost <= GameManager.instance.coins)
        {
            GameObject turret = Instantiate(turretPrefab, turretPosition.position, turretPosition.rotation) as GameObject;
            turretPlaced[turretNumber] = turret;
            GameManager.instance.coins -= turretPrefab.GetComponent<TurretShooting>().cost;
            GameManager.instance.coinsText.text = "" + GameManager.instance.coins;
            turretTypes[turretNumber] = currentTurretType;
            buttonsBaseTurret.SetActive(false);
            buttonsTurretPlaced.SetActive(true);
            buttonFather.SetActive(false);
            //Destroy(buttonFather.gameObject);
        }
    }
    public void AssignTurret(GameObject turret)
    {
        buttonFather = turret;
    }
    public void DeselectMenu()
    {
        buttonsBaseTurret.SetActive(false);
    }

    public void SelectTurret(GameObject Buttons)
    {
        //for (int i = 0; i < buttonsArray.Length; i++)
        //    buttonsArray[i].gameObject.SetActive(false);
        Buttons.gameObject.SetActive(true);
        buttonsBaseTurret = Buttons;
    }

    public void AssignTurretPlacedButton(GameObject Buttons)
    {
        buttonsTurretPlaced = Buttons;
    }

    public void TurretNumber(int turretPositionInTheArray)
    {
        turretNumber = turretPositionInTheArray;
    }
    public void AssingTurretType(int turretType)
    {
        currentTurretType = turretType;
    }
    public void ChooseTurret(GameObject Turret)
    {
        turretPrefab = Turret;
    }
    public void SpawnFighterPosition(Transform Position)
    {
        fighterTurretSpawn = Position;
    }
    public void SpawnFighterTurret(Transform turretPosition)
    {
        if (turretPrefab.GetComponent<TurretShooting>().cost <= GameManager.instance.coins)
        {
            turretTypes[turretNumber] = currentTurretType;
            GameObject turret = Instantiate(turretPrefab, turretPosition.position, turretPosition.rotation) as GameObject;
            turretPlaced[turretNumber] = turret;
            turret.GetComponent<TurretSpawner>().spawnPoint = fighterTurretSpawn;
            GameManager.instance.coins -= turretPrefab.GetComponent<TurretSpawner>().cost;
            GameManager.instance.coinsText.text = "" + GameManager.instance.coins;
            buttonsBaseTurret.SetActive(false);
            buttonsTurretPlaced.SetActive(true);
            buttonFather.SetActive(false);
        }
    }
    public void TurretUpgrade(Transform turretPosition)
    {
        switch(turretTypes[turretNumber])
        {
            case 1:
                if (turretPrefabUpgrades[0].GetComponent<TurretShooting>().cost <= GameManager.instance.coins)
                {
                    Destroy(turretPlaced[turretNumber].gameObject);
                    Instantiate(turretPrefabUpgrades[0], turretPosition.position, turretPosition.rotation);
                    GameManager.instance.coins -= turretPrefab.GetComponent<TurretShooting>().cost;
                    GameManager.instance.coinsText.text = "" + GameManager.instance.coins;
                    //turretTypes[turretNumber] = currentTurretType;
                    buttonsBaseTurret.SetActive(false);
                }
                break;
            case 2:
                if (turretPrefabUpgrades[1].GetComponent<TurretShooting>().cost <= GameManager.instance.coins)
                {
                    Destroy(turretPlaced[turretNumber].gameObject);
                    Instantiate(turretPrefabUpgrades[1], turretPosition.position, turretPosition.rotation);
                    GameManager.instance.coins -= turretPrefab.GetComponent<TurretShooting>().cost;
                    GameManager.instance.coinsText.text = "" + GameManager.instance.coins;
                    //turretTypes[turretNumber] = currentTurretType;
                    buttonsBaseTurret.SetActive(false);
                }
                break;
            case 3:
                if (turretPrefabUpgrades[2].GetComponent<TurretShooting>().cost <= GameManager.instance.coins)
                {
                    Destroy(turretPlaced[turretNumber].gameObject);
                    Instantiate(turretPrefabUpgrades[2], turretPosition.position, turretPosition.rotation);
                    GameManager.instance.coins -= turretPrefab.GetComponent<TurretShooting>().cost;
                    GameManager.instance.coinsText.text = "" + GameManager.instance.coins;
                    //turretTypes[turretNumber] = currentTurretType;
                    buttonsBaseTurret.SetActive(false);
                }
                break;
            case 4:
                if (turretPrefabUpgrades[3].GetComponent<TurretShooting>().cost <= GameManager.instance.coins)
                {
                    Destroy(turretPlaced[turretNumber].gameObject);
                    //turretTypes[turretNumber] = currentTurretType;
                    GameObject turret = Instantiate(turretPrefabUpgrades[3], turretPosition.position, turretPosition.rotation) as GameObject;
                    turret.GetComponent<TurretSpawner>().spawnPoint = fighterTurretSpawn;
                    GameManager.instance.coins -= turretPrefab.GetComponent<TurretSpawner>().cost;
                    GameManager.instance.coinsText.text = "" + GameManager.instance.coins;
                    buttonsBaseTurret.SetActive(false);
                }
                break;
        }
    }
}
