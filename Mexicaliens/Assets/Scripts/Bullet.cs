﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public bool areaDamage;
    private float timer;
    public float timeOfDestroy;
    public float velocity;
    public float damage;
    public float range;

    public LayerMask enemies;
    public Collider[] colliders;
    private Rigidbody rb;
    public GameObject target;
    
    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    //the bullet goes toward the target

	void Update ()
    {
        Timer();
        //if (target = null)
        //    Destroy(this.gameObject);
        transform.position += transform.forward * velocity *Time.deltaTime;
        transform.LookAt(target.transform);
        
	}

    //the bullets destroy himself after some time

    void Timer()
    {
        timer += Time.deltaTime;
        if (timer >= timeOfDestroy)
            Destroy(this.gameObject);
    }

    //the bullet is destroyed once it hits something,if the bullet does AoE damage,it uses a OverlapSphere to deal damage to all enemies in range

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag != "Bullet")
        {
            if (!areaDamage)
            {
                collision.collider.GetComponent<Enemies>().hp -= damage;
            }
            else if (areaDamage)
            {
                colliders = Physics.OverlapSphere(transform.position, range, enemies);
                for (int i = 0; i < colliders.Length; i++)
                    colliders[i].gameObject.GetComponent<Enemies>().hp -= damage;
            }
            Destroy(this.gameObject);
        }
    }
}
