﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretSpawner : MonoBehaviour
{
    public bool unitSpawned = false;
    public int cost;
    public float timer = 0;
    public float rateOfSpawn;
    public Transform spawnPoint;
    public GameObject FighterPrefab;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!unitSpawned)
        {
            Timer();
            if (timer >= rateOfSpawn)
                Spawn();
        }
    }
    void Spawn()
    {
        GameObject fighter = Instantiate(FighterPrefab, spawnPoint.transform.position, spawnPoint.transform.rotation) as GameObject;
        fighter.GetComponent<FighterActions>().TurretSpawner = this.gameObject;
        unitSpawned = true;
        timer = 0;
    }
    void Timer()
    {
        timer += Time.deltaTime;
    }
}
